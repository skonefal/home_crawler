/* accepts parameters
 * h  Object = {h:x, s:y, v:z}
 * OR 
 * h, s, v
*/
function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (h && s === undefined && v === undefined) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.floor(r * 255),
        g: Math.floor(g * 255),
        b: Math.floor(b * 255)
    };
}

/////////FORMAT String
// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {

    var args = arguments;
    var sprintfRegex = /\{(\d+)\}/g;

    var sprintf = function (match, number) {
      return number in args ? args[number] : match;
    };

    return this.replace(sprintfRegex, sprintf);
  };
}
/////END
function number2Hex(num)
{
    val = num.toString(16).toUpperCase();
    if (val.length == 1)
    {
        return '0'+val;
    }
    return val;
}

function price2color(price_per_meter)
{
    var max = 7000;
    var min = 3000;
    var value
    if (price_per_meter < min)
    {
        value = 1.0;
    }
    else if (price_per_meter > max)
    {
        value = 0.0;
    }
    else
    {
        var nominator = price_per_meter - min;
        var denominator = max - min;

        value = 1.0 - (nominator/denominator);        
    }

    var rgb = HSVtoRGB(0.41, 1.0, value);
    
    return number2Hex(rgb.r) + number2Hex(rgb.g) + number2Hex(rgb.b);
}

function removeDecimal(number)
{
    var decIdx = number.indexOf(",")
    if (decIdx != -1)
    {
        number = number.substring(0, decIdx);
    }
    return number;
}

function prepareDescription(ogl)
{
   var infoWindowHTML = '<table class="table table-bordered"><tr><th>Cena</th><th>Za metr</th><th>Link</th></tr><tr><td>{2}</td><td>{3}</td><td><a href="{0}" target="_blank">{1}</a></td>';
   return infoWindowHTML.format(String(ogl.url), String(ogl.description), ogl.price, ogl.price_per_sq_met)
}
