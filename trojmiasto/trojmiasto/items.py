# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class Ogloszenie(Item):
    url = Field()
    name = Field()
    description = Field()
    price = Field()
    price_per_sq_met = Field()
    area = Field()
    no_of_rooms = Field()
    address = Field()
    district = Field()
    latitude = Field()
    longitude = Field()
    
    

