from scrapy.spider import Spider
from scrapy.selector import Selector
from scrapy.http import Request

from pygeocoder import Geocoder
from pygeolib import GeocoderError
#from geopy import geocoders
from unidecode import unidecode

from trojmiasto.items import Ogloszenie

def get_first(iterable, default=None):
    for item in iterable:
        return item.extract()
    return default

def get_string_xpath(page, xpath):
    return get_first(page.xpath(xpath), '')

class TrojmiastoSpider(Spider):
    name = 'trojmiasto'
    allowed_domains = ['trojmiasto.pl']
    #stare start_urls = ['http://ogloszenia.trojmiasto.pl/nieruchomosci-sprzedam/?formSended=1&formSendedFrom=advSearchLeft&searchFormSended=1&id_kat=101&katlist=1&id_kat_list%5B101%5D=101&id_kat_list%5B106%5D=106&rodzaj_nieruchomosci=100&cena_min=150000&cena_max=300000&cenam2_min=&cenam2_max=&powierzchnia_min=&powierzchnia_max=&adres_ulica_i_nr=&districtListWhatSelected=wybranych%3A+28&f1i%5B0%5D=&e1i=33%7C38%7C58%7C69%7C41%7C79%7C40%7C42%7C46%7C35%7C3%7C68%7C34%7C32%7C45%7C1%7C43%7C87%7C5%7C86%7C119%7C2%7C140%7C139%7C89%7C7%7C135%7C31&f1i=&x=29&y=5&l_pokoi_min=2&l_pokoi_max=&pietro_min=&pietro_max=&l_pieter_min=&l_pieter_max=&rok_budowy_min=&rok_budowy_max=&powierzchnia_dzialki_min=&powierzchnia_dzialki_max=&typ_ogrzewania=&slowa_option=all_phrases&slowa=&obList=&data_wprow=all&order=added+desc&limit=100&id_kat=&limit=100']
    #3 i 4 pokoje start_urls = ['http://ogloszenia.trojmiasto.pl/nieruchomosci-sprzedam/?formSended=1&formSendedFrom=advSearchLeft&searchFormSended=1&id_kat=101&katlist=1&id_kat_list%5B101%5D=101&rodzaj_nieruchomosci=&cena_min=&cena_max=320000&cenam2_min=&cenam2_max=&powierzchnia_min=&powierzchnia_max=&adres_ulica_i_nr=&districtListWhatSelected=wybranych%3A+13&f1i%5B0%5D=&e1i=38%7C79%7C35%7C3%7C34%7C36%7C5%7C119%7C2%7C140%7C139%7C72%7C89&f1i=&x=43&y=14&l_pokoi_min=3&l_pokoi_max=&pietro_min=&pietro_max=&l_pieter_min=&l_pieter_max=&rok_budowy_min=&rok_budowy_max=&powierzchnia_dzialki_min=&powierzchnia_dzialki_max=&typ_ogrzewania=&slowa_option=all_phrases&slowa=&obList=&data_wprow=all&order=data_wazne_SMS+DESC%2C+data_wprow+DESC&limit=20&id_kat=&limit=20']
    #start_urls = ['http://ogloszenia.trojmiasto.pl/nieruchomosci-sprzedam/?formSended=1&formSendedFrom=advSearchLeft&searchFormSended=1&id_kat=101&katlist=1&id_kat_list%5B101%5D=101&rodzaj_nieruchomosci=&cena_min=&cena_max=300000&cenam2_min=&cenam2_max=&powierzchnia_min=&powierzchnia_max=&adres_ulica_i_nr=&districtListWhatSelected=wybranych%3A+27&f1i%5B0%5D=&e1i=33%7C38%7C69%7C41%7C79%7C46%7C35%7C3%7C34%7C32%7C45%7C120%7C1%7C43%7C36%7C87%7C5%7C55%7C86%7C119%7C2%7C140%7C139%7C72%7C89%7C7%7C135&f1i=&x=46&y=8&l_pokoi_min=3&l_pokoi_max=3&pietro_min=&pietro_max=&l_pieter_min=&l_pieter_max=&rok_budowy_min=&rok_budowy_max=&powierzchnia_dzialki_min=&powierzchnia_dzialki_max=&typ_ogrzewania=&slowa_option=all_phrases&slowa=&obList=&data_wprow=all&order=data_wazne_SMS+DESC%2C+data_wprow+DESC&limit=20&id_kat=&limit=20']
    start_urls = ['http://ogloszenia.trojmiasto.pl/nieruchomosci-sprzedam/?formSended=1&formSendedFrom=advSearchLeft&searchFormSended=1&id_kat=101&katlist=1&id_kat_list%5B101%5D=101&rodzaj_nieruchomosci=100&cena_min=&cena_max=&cenam2_min=&cenam2_max=&powierzchnia_min=50&powierzchnia_max=70&adres_ulica_i_nr=&districtListWhatSelected=Gda%F1sk+Morena&f1i%5B0%5D=&e1i=3&f1i=&x=16&y=9&l_pokoi_min=3&l_pokoi_max=3&pietro_min=&pietro_max=&l_pieter_min=&l_pieter_max=&rok_budowy_min=&rok_budowy_max=&powierzchnia_dzialki_min=&powierzchnia_dzialki_max=&typ_ogrzewania=&slowa_option=all_phrases&slowa=&obList=&data_wprow=all&order=data_wazne_SMS+DESC%2C+data_wprow+DESC&limit=20&id_kat=&limit=20']

    def parse(self, response):
        sel = Selector(response)
        ogloszenia = sel.xpath("//div[contains(@class,'adv2-item')]")
        nastepna = get_string_xpath(sel, "//a[@title='" + u'nast\u0119pna' + "']/@href")
        if nastepna != '':
            yield Request('http://ogloszenia.trojmiasto.pl/nieruchomosci-sprzedam/' + nastepna)
        for ogl in ogloszenia:
            item = Ogloszenie()
            item['url'] = get_string_xpath(ogl, "div[@class='adv2-head']/p[@class='title']/a/@href")
            item['name'] = get_string_xpath(ogl, "div[@class='adv2-head']/p[@class='title']/a/text()")
            item['description'] = get_string_xpath(ogl, "div[@class='adv2-content']/p[@class='adv2-desc']/text()").lstrip().rstrip()
            item['price'] = filter(lambda x: x.isdigit(), get_string_xpath(ogl, "div[@class='adv2-content']/div[@class='adv2-infobar']/strong/text()"))
            price_text = ogl.xpath("div[@class='adv2-content']/div[@class='adv2-infobar']/text()")
            if len(price_text) > 2:
                item['price_per_sq_met'] =  filter(lambda x: x.isdigit(), price_text[1].extract())
            item['address'] = get_string_xpath(ogl, "div[@class='adv2-content']/div[@class='adv2-params']/ul/li[4]/text()")
            item['area'] = filter(lambda x: x.isdigit() or x == ',', get_string_xpath(ogl, "div[@class='adv2-content']/div[@class='adv2-params']/ul/li[1]/text()"))
            item['no_of_rooms'] = filter(lambda x: x.isdigit(), get_string_xpath(ogl, "div[@class='adv2-content']/div[@class='adv2-params']/ul/li[2]/text()"))
            item['district'] = get_string_xpath(ogl, "div[@class='adv2-head']/p[@class='place']/text()").lstrip().rstrip()

            location = unidecode(item['address'] + u', Gdansk, Polska')
            try:
                results = Geocoder.geocode(location)
                item['latitude'] = results[0].coordinates[0]
                item['longitude'] = results[0].coordinates[1]
            except GeocoderError:
                print 'Cannot find location: ' + location
            yield item
